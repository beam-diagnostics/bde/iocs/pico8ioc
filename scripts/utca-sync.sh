#!/bin/bash

cat << EOF > /tmp/exclude
.git
O.*
.built
.gitignore
EOF

folders="adpico8-master pico8app-master pico8ioc-master"
[[ -z $1 ]] || folders="adpico8-master"

for i in $folders; do
  rsync -av --exclude-from=/tmp/exclude $HOME/bde/R3.15.5/$i dev@bd-cpu13.cslab.esss.lu.se:\$HOME/bde/R3.15.5 || exit 11
done

uids="adpico8:master pico8app:master pico8ioc:master"
[[ -z $1 ]] || uids="adpico8:master"

for i in $uids; do
  #ssh -t dev@bd-cpu13.cslab.esss.lu.se \$HOME/.local/bin/bde-dev provide -u $i -f || exit 22
  #ssh -t dev@bd-cpu13.cslab.esss.lu.se \$HOME/.local/bin/bde-dev clean -u $i -f || exit 22
  #ssh -t dev@bd-cpu13.cslab.esss.lu.se \$HOME/.local/bin/bde-dev distclean -u $i -f || exit 22
  ssh -t dev@bd-cpu13.cslab.esss.lu.se \$HOME/.local/bin/bde-dev build -u $i -f || exit 22
done
